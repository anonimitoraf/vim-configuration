#!/usr/bin/env bash

if [ -f ~/.vimrc ]; then
  date_now="$(date -u +"%Y-%m-%dT%H:%M:%SZ")"
  new_name=".vimrc_backup_${date_now}"

  echo "Backing up as ${new_name} then deleting existing .vimrc"
  mv ~/.vimrc "${new_name}"
fi

echo "Making a hardlink for updated .vimrc"
ln .vimrc ~/.vimrc

pushd bundle/vimproc.vim

# Upon building vimproc, I encountered an error that's due to lib dir not existing
if [ ! -d lib ]; then
  mkdir lib
fi

# compile vimproc
make

popd
